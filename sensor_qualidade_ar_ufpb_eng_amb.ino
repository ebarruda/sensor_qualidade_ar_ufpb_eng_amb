/*
UFPB, Engenharia Ambiental 2019.2 

Projeto : Dispositivo feito com Arduino Nano e sensore MQ2, MQ7 e MQ135, para calcular os níveis dos gases poluentes, em ppm, no ar atmosférico da cidade de João Pessoa. 

Artigo: Monitoramento da qualidade do ar por meio de sensoriamento remoto

Alunos : 
JEFFERSON LUIZ - Matrícula: 20190XXXX70
GIULLIANA GOMES - Matrícula: 20190XXXX97
PEDRO ANDRÉ - Matrícula: 20190XXXX36
TUANY KEROLAYNE - Matrícula: 20190XXXX52

MQ-2
– Detecção de fumaça e gases inflamáveis: GLP, Metano, Propano, Butano, Hidrogênio, Álcool, Gás Natural e outros inflamáveis.
www.haoyuelectronics.com/Attachment/MQ-2/MQ-2.pdf

MQ-7
– Detecção do gás Monóxido de Carbono
https://www.sparkfun.com/datasheets/Sensors/Biometric/MQ-7.pdf

MQ-135
– Detecção de gases tóxicos como amônia, dióxido de carbono, benzeno, óxido nítrico, fumaça e álcool
https://www.olimex.com/Products/Components/Sensors/SNS-MQ135/resources/SNS-MQ135.pdf

Outras referências

Sensoriamento de gases em tempo real através de sensores MQ-(0-9)
https://jualabs.wordpress.com/2016/11/30/sensoriamento-de-gases-em-tempo-real-atraves-de-sensores-mq-0-9/

MQ gas sensor correlation function estimation:
http://davidegironi.blogspot.com/2017/05/mq-gas-sensor-correlation-function.html#.Xi-FjyNv-Ul
*/

#include "Arduino.h"
#include "SD.h"
 
#define pinoAnalogicoMQ2 A2
#define pinoDigitalMQ2 4
#define ledMQ2 7

#define pinoAnalogicoMQ7 A1
#define pinoDigitalMQ7 3
#define ledMQ7 6

#define pinoAnalogicoMQ135 A0
#define pinoDigitalMQ135 2
#define ledMQ135 5

#define somPino 9

#define sdPinoArquivoCS 10
 
int valorAnalogMQ2;
int valorDigitMQ2;

int valorAnalogMQ7;
int valorDigitMQ7;

int valorAnalogMQ135;
int valorDigitMQ135;

long anteriorSegundoMillis = 0;
long intervaloSegundoMillis = 1000;

long anteriorMinutoMillis = 0;
long intervaloMinutoMillis = 60000;

const int pinoBotao = 8;

int tocarSom = 0;

File arquivoCSV;

void setup() {
   Serial.begin(9600);

   pinMode(pinoBotao, INPUT_PULLUP);
   
   pinMode(pinoAnalogicoMQ2, INPUT);
   pinMode(pinoDigitalMQ2, INPUT);
   pinMode(ledMQ2, OUTPUT);
   
   pinMode(pinoAnalogicoMQ7, INPUT);
   pinMode(pinoDigitalMQ7, INPUT);
   pinMode(ledMQ7, OUTPUT);
   
   pinMode(pinoAnalogicoMQ135, INPUT);
   pinMode(pinoDigitalMQ135, INPUT);
   pinMode(ledMQ135, OUTPUT);
   
   pinMode(sdPinoArquivoCS, OUTPUT);
   
   if (!SD.begin()) {
     Serial.println(F("Falha ao inicializar SD."));
   } else {
     arquivoCSV = SD.open("dados.csv", FILE_WRITE);
   
     if (arquivoCSV) {
       arquivoCSV.print("minuto,");
       arquivoCSV.print("valor_mq135,");
       arquivoCSV.print("detectado_mq135,");
       arquivoCSV.print("valor_mq7,");
       arquivoCSV.print("detectado_mq7,");
       arquivoCSV.print("valor_mq2,");
       arquivoCSV.println("detectado_mq2");
      
       arquivoCSV.close();
     } else {
       Serial.println(F("Erro ao abrir arquivo."));
     }
   }
   
   if (digitalRead(pinoBotao) == LOW) {
     tone(somPino, 1000, 500);
   }
}
 
void loop() {
  unsigned long atualMillis = millis();
  
  if (atualMillis - anteriorSegundoMillis > intervaloSegundoMillis) {
    anteriorSegundoMillis = atualMillis;
  
    valorAnalogMQ2 = analogRead(pinoAnalogicoMQ2); 
    valorDigitMQ2  = digitalRead(pinoDigitalMQ2);
   
    valorAnalogMQ7 = analogRead(pinoAnalogicoMQ7); 
    valorDigitMQ7  = digitalRead(pinoDigitalMQ7);
   
    valorAnalogMQ135 = analogRead(pinoAnalogicoMQ135); 
    valorDigitMQ135  = digitalRead(pinoDigitalMQ135);

    Serial.print(atualMillis / 1000);
    Serial.print(",");
    Serial.print(valorAnalogMQ135);
    Serial.print(",");
    Serial.print(valorDigitMQ135);
    Serial.print(",");
    Serial.print(valorAnalogMQ7);
    Serial.print(",");
    Serial.print(valorDigitMQ7);
    Serial.print(",");
    Serial.print(valorAnalogMQ2);
    Serial.print(",");
    Serial.print(valorDigitMQ2);

    if (atualMillis - anteriorMinutoMillis > intervaloMinutoMillis) {
      anteriorMinutoMillis = atualMillis;
      
      Serial.println(" *");
      
      arquivoCSV = SD.open("dados.csv", FILE_WRITE);
   
      if (arquivoCSV) {
        arquivoCSV.print(atualMillis / 60000);
        arquivoCSV.print(",");
        arquivoCSV.print(valorAnalogMQ135);
        arquivoCSV.print(",");
        arquivoCSV.print(valorDigitMQ135);
        arquivoCSV.print(",");
        arquivoCSV.print(valorAnalogMQ7);
        arquivoCSV.print(",");
        arquivoCSV.print(valorDigitMQ7);
        arquivoCSV.print(",");
        arquivoCSV.print(valorAnalogMQ2);
        arquivoCSV.print(",");
        arquivoCSV.println(valorDigitMQ2);
      
        arquivoCSV.close();
      } else {
        Serial.println(F("Erro ao abrir arquivo."));
      }
    } else {
      Serial.println("");
    }
    
    if (digitalRead(pinoBotao) == LOW) {
      if (valorDigitMQ135 == 0) {
        digitalWrite(ledMQ135, LOW);
      } else {
        digitalWrite(ledMQ135, HIGH);

        tocarSom = 2;
      }
      
      if (valorDigitMQ7 == 0) {
        digitalWrite(ledMQ7, LOW);
      } else {
        digitalWrite(ledMQ7, HIGH);

        tocarSom = 2;
      }
      
      if (valorDigitMQ2 == 0) {
        digitalWrite(ledMQ2, LOW);
      } else {
        digitalWrite(ledMQ2, HIGH);

        tocarSom = 2;
      }

      if (tocarSom > 0) {
        tone(somPino, 1000, 500);

        --tocarSom;
      }
    } else {
     if (digitalRead(ledMQ135) == HIGH) { digitalWrite(ledMQ135, LOW); }
     if (digitalRead(ledMQ7) == HIGH) { digitalWrite(ledMQ7, LOW); }
     if (digitalRead(ledMQ2) == HIGH) { digitalWrite(ledMQ2, LOW); }
    }
  }
}
